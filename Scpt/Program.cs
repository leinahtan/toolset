﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CommandLine;
using Core;
using log4net;
using log4net.Config;

namespace Scpt
{
    internal class Program
    {
        private static ILog Log { get; } = LogManager.GetLogger(typeof(Program));
        private static Options Options { get; } = new Options();
        private static Batch Batch { get; } = new Batch();

        static Program()
        {
            XmlConfigurator.Configure();
        }

        private static int Main(string[] args)
        {
            try
            {
                Parser.Default.ParseArgumentsStrict(args, Options);

                if (Options.NothingToDo)
                {
                    return Warning("Expecting at least one action (list, commands, kill, delete, uninstall-timeout)");
                }

                var services = QueryServices(Options.Query);

                if (!services.Any()) return Warning("No services found");

                if (Options.States.Any())
                {
                    const StringComparison x = StringComparison.CurrentCultureIgnoreCase;
                    services = services.Where(s => Options.States.Any(t => string.Equals(t, s.State.GetName(), x)))
                        .ToList();
                }

                if (!services.Any())
                {
                    var states = string.Join(" or ", Options.States.Select(s => $"'{s}'"));
                    return Warning($"No services found with state {states}");
                }

                if (Options.List) List(services);

                if (Options.Commands.Any()) Task.WaitAll(services.Select(Command).ToArray());

                if (Options.Kill) Task.WaitAll(services.Select(Kill).ToArray());

                if (Options.Delete) Task.WaitAll(services.Select(Delete).ToArray());

                if (Options.UninstallTimeout != 0) WaitForUninstall(Options.UninstallTimeout);

                return 0;
            }
            catch (Exception e)
            {
                Log.Error($"Error: {e}");

                return -1;
            }
            finally
            {
                Log.Info("Finished");
            }
        }

        private static void WaitForUninstall(int timeout)
        {
            var source = timeout < 0 ? new CancellationTokenSource() : new CancellationTokenSource(timeout);
            var token = source.Token;
            var stopwatch = Stopwatch.StartNew();
            var ok = false;

            Action action = () =>
            {
                List<Service> services;

                try
                {
                    services = QueryServices(Options.Query);
                }
                catch (Exception e)
                {
                    Log.Warn($"Query: {e}");
                    return;
                }

                if (services.Any())
                {
                    Log.Info($"  {services.Count} services are still present after {stopwatch.ElapsedMilliseconds}ms");
                    if (Options.Verbose) services.ForEach(s => Log.Info($"  {{{s.ToString()}}}"));
                }
                else
                {
                    Log.Info($"All services uninstalled after {stopwatch.ElapsedMilliseconds}ms");
                    ok = true;
                    source.Cancel();
                    stopwatch.Stop();
                }
            };

            var task = Task.Run(() =>
            {
                token.ThrowIfCancellationRequested();

                while (!token.IsCancellationRequested)
                {
                    action();

                    Task.Delay(2000, token).Wait(token);
                }
            }, token);

            Log.Info($"Waiting for services to completely uninstall (Timeout: {(timeout < 0 ? "None" : $"{timeout}ms")})");

            try
            {
                task.Wait(token);
            }
            catch (OperationCanceledException)
            {
                if (stopwatch.ElapsedMilliseconds > timeout) Log.Warn($"Timeout reached after {stopwatch.ElapsedMilliseconds}ms");
            }
            catch (AggregateException e)
            {
                Log.Warn($"Unknown error: {e}");
            }

            if (!ok) Log.Warn("Some services could still be present");
        }

        private static List<Service> QueryServices(string query)
        {
            var serviceNames = new List<string>();

            Batch.Evaluate($"sc query state= all | findstr /r {query} | findstr SERVICE_NAME:",
                           s => serviceNames.Add(s.Substring(13).Trim()));

            var tasks = serviceNames.Select(QueryService).ToArray();

            Task.WaitAll(tasks);

            var services = tasks.Select(t => t.Result).ToList();

            if (Options.Verbose) Log.Info($"Found {services.Count} services");

            return services;
        }

        private static async Task<Service> QueryService(string name)
        {
            Action<string, string, Action<string>> extract = (input, test, then) =>
            {
                var tokens = input.Split(new[] { ':' }, 2);
                if (tokens.Length != 2) return;
                if (tokens[0].Trim().ToLower() == test) then(tokens[1].Trim());
            };

            string state = null;
            string pid = null;
            string path = null;

            await Evaluate($"sc queryex \"{name}\"", x =>
            {
                extract(x, "pid", v => pid = v);
                extract(x, "state", v => state = v.Split(new[] { ' ' }, 2)[0].Trim());
            });

            try
            {
                await Evaluate($"sc qc \"{name}\"",
                               x => extract(x, "binary_path_name",
                               v => path = v[0] == '"'
                                    ? v.Substring(1, v.IndexOf('"', 1) - 1) : v.Split(new[] { ' ' }, 2)[0]));
            }
            catch (Exception)
            {
                // binary can be null (for example when the service is disabled)
            }

            return new Service(name, state, pid, path);
        }

        private static int Warning(params string[] lines)
        {
            foreach (var line in lines) Log.Warn(line);

            return 0;
        }

        #region Actions

        private static void List(IList<Service> services)
        {
            var longestPid = services.Max(s => s.Pid.Length);
            var longestState = services.Max(s => s.State.GetName().Length);
            var longestName = services.Max(s => s.Name.Length);

            Action(services, service =>
            {
                var pid = service.Pid.PadLeft(longestPid);
                var state = service.State.GetName().PadRight(longestState);
                var name = service.Name.PadRight(longestName);

                Log.Info($"  {pid} - [{state}] {name} \"{service.Path}\"");
            });
        }

        private static void Command(IEnumerable<Service> services) => Action(services, service =>
        {
            foreach (var command in Options.Commands)
            {
                Log.Info($"[{service.Name}]: Sending '{command}'");

                if (Options.Verbose)
                {
                    Log.Info($"[{service.Name}]: Binary -> '{service.File.FullName}'");
                }

                if (!service.File.Exists) throw new Exception("Binary not found");

                var exit = Batch.Evaluate($"{service.File.FullName} {command}");

                if(exit != 0) throw new Exception($"Non zero exit ({exit})");
            }
        });

        private static async Task Command(Service service)
        {
            foreach (var command in Options.Commands)
            {
                Log.Info($"[{service.Name}]: Sending '{command}'");

                if (Options.Verbose)
                {
                    Log.Info($"[{service.Name}]: Binary -> '{service.File.FullName}'");
                }

                if (!service.File.Exists) throw new Exception("Binary not found");

                await Evaluate($"{service.File.FullName} {command}");
            }
        }

        private static void Delete(IEnumerable<Service> services) => Action(services, service =>
        {
            Log.Info($"[{service.Name}]: Deleting");

            var exit = Batch.Evaluate($"sc delete {service.Name}");

            if(exit != 0) throw new Exception($"Non zero exit ({exit})");
        });

        private static async Task Delete(Service service)
        {
            Log.Info($"[{service.Name}]: Deleting");

            await Evaluate($"sc delete {service.Name}");
        }

        private static void Kill(IEnumerable<Service> services)
            => Action(services.Where(s => s.Pid != "0"), service =>
        {
            Log.Info($"[{service.Name}]: Killing pid '{service.Pid}'");

            var exit = Batch.Evaluate($"taskkill /f /pid {service.Pid}");

            if(exit != 0) throw new Exception($"Non zero exit ({exit})");
        });

        private static async Task Kill(Service service)
        {
            Log.Info($"[{service.Name}]: Killing pid '{service.Pid}'");

            await Evaluate($"taskkill /f /pid {service.Pid}");
        }

        private static void Action(IEnumerable<Service> services, Action<Service> action)
        {
            foreach (var service in services)
            {
                try
                {
                    action(service);
                }
                catch (Exception e)
                {
                    Log.Warn($"[{service.Name}]: {e.Message}");
                }
            }
        }

        #endregion

        private static async Task Evaluate(string cmd) => await Evaluate(() => Batch.Evaluate(cmd));

        private static async Task Evaluate(string cmd, Action<string> onOutput)
        {
            await Evaluate(() => Batch.Evaluate(cmd, onOutput));
        }

        private static async Task Evaluate(Func<int> action)
        {
            var exit = await Task.Run(action);

            if (exit != 0) throw new Exception($"Non zero exit ({exit})");
        }
    }

    public static class TaskExtensions
    {
        public static void ExecuteSynchronously(this Task task)
        {
            task.ContinueWith(t => 0).ExecuteSynchronously();
        }

        public static T ExecuteSynchronously<T>(this Task<T> task)
        {
            return Task.Run(async () => await task).Result;
        }
    }
}
