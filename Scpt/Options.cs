﻿using System.Collections.Generic;
using System.Linq;
using CommandLine;
using CommandLine.Text;

namespace Scpt
{
    public class Options
    {
        [Option('q', "query", Required = true, HelpText = "Specify the query used to find services")]
        public string Query { get; set; }

        [OptionList('s', "states", HelpText = "Specify a whitelist of services states" )]
        public IList<string> States { get; set; } = new List<string>();

        [OptionList('c', "commands", HelpText = "Specify commands to send to the services")]
        public IList<string> Commands { get; set; } = new List<string>();

        [Option('l', "list", HelpText = "List the services found")]
        public bool List { get; set; }

        [Option('k', "kill", HelpText = "Kill the found services processes")]
        public bool Kill { get; set; }

        [Option('d', "delete", HelpText = "Delete the services found")]
        public bool Delete { get; set; }

        [Option('t', "uninstall-timeout", DefaultValue = 0, HelpText = "If set, wait for services to be completely uninstalled or timeout after x milliseconds. (-1 to wait indefinitly)")]
        public int UninstallTimeout { get; set; }

        [Option('v', "verbose", HelpText = "Print more details during execution")]
        public bool Verbose { get; set; }

        [HelpOption]
        public string GetUsage() => HelpText.AutoBuild(this, c => HelpText.DefaultParsingErrorsHandler(this, c));

        public bool NothingToDo => !(List || Commands.Any() || Kill || Delete || UninstallTimeout != 0);
    }
}