﻿using System;
using System.IO;

namespace Scpt
{
    public class Service
    {
        public string Name { get; }
        public State State { get; }
        public string Pid { get; }
        public string Path { get; }

        public Service(string name, string pid, string path)
        {
            Name = name ?? string.Empty;
            Pid = pid ?? string.Empty;
            Path = path ?? string.Empty;
        }

        public Service(string name, State state, string pid, string path) : this(name, pid, path)
        {
            State = state;
        }

        public Service(string name, string state, string pid, string path) : this(name, pid, path)
        {
            try
            {
                State = (State) int.Parse(state);
            }
            catch (Exception)
            {
                State = State.Unknown;
            }
        }

        public FileInfo File => new FileInfo(Path);

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(State)}: {State}, {nameof(Pid)}: {Pid}";
        }
    }

    public enum State
    {
        Unknown = 0,

        Stopped = 1,
        StartPending = 2,
        StopPending = 3,
        Running = 4,
        ContinuePending = 5,
        PausePending = 6,
        Paused = 7
    }

    public static class StateExtensions
    {
        public static string GetName(this State state) => Enum.GetName(typeof(State), state);
    }
}
