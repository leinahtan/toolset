﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CommandLine;
using Core;
using log4net;
using log4net.Config;

namespace Parallel
{
    internal class Program
    {
        private static ILog Log { get; } = LogManager.GetLogger(typeof(Program));
        private static Options Options { get; } = new Options();
        private static Batch Batch { get; } = new Batch();

        private static string CommonPrefix { get; set; }
        private static int MaxCommandLength { get; set; }

        static Program()
        {
            XmlConfigurator.Configure();
        }

        private static void Main(string[] args)
        {
            Parser.Default.ParseArgumentsStrict(args, Options);

            var commands = Options.Commands.ToArray();

            MaxCommandLength = commands.Select(c => c.Length).Max();

            Log.Info(commands.Any() ? $"Sending {string.Join(", ", commands)}" : "Executing");

            var paths = Options.Executables
                .Select(exe => string.IsNullOrWhiteSpace(Options.Format) ? exe : string.Format(Options.Format, exe))
                .ToList();

            CommonPrefix = new string(paths.First().Substring(0, paths.Min(p => p.Length))
                                                   .TakeWhile((c, i) => paths.All(p => p[i] == c))
                                                   .ToArray());

            Log.Info($"in {CommonPrefix}");

            foreach (var path in paths)
            {
                Log.Info($@"  {path.Substring(CommonPrefix.Length)}");
            }

            var tasks = paths.Select(path => Execute(path, commands));

            Task.WaitAll(tasks.ToArray());

            Log.Info("Finished");
        }

        private static async Task Execute(string path, params string[] commands)
        {
            if (!File.Exists(path))
            {
                Log.Warn($"Executable not found: '{path.Substring(CommonPrefix.Length)}'");

                return;
            }

            if (!commands.Any())
            {
                if (Options.Verbose) Log.Info($"Executing \"{path.Substring(CommonPrefix.Length)}\"");

                await Run(() => Batch.Evaluate($"{path}"));

                return;
            }

            foreach (var command in commands)
            {
                var line = $"'{command.PadLeft(MaxCommandLength)}' to \"{path.Substring(CommonPrefix.Length)}\"";

                if (Options.Verbose) Log.Info($"Sending {line}");

                await Run(() => Batch.Evaluate($"{path} {command}"));

                //if (Options.Verbose) Log.Info($"\"{line}\" - Success");
            }
        }

        private static async Task Run(Func<int> batch)
        {
            var exit = await Task.Run(batch);

            if (exit != 0) throw new Exception($"Non zero exit ({exit})");
        }
    }
}
