﻿using System.Reflection;
using System.Runtime.InteropServices;
using CommandLine;

[assembly: AssemblyTitle("Parallel")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Parallel")]
[assembly: AssemblyCopyright("Copyright © 2016 Nathaniel Piché")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyLicense("This is free software. You may redistribute copies of it under the terms of",
                           "the MIT License <http://www.opensource.org/licenses/mit-license.php>.")]

[assembly: AssemblyUsage("\nUsage: parallel -e {EXECUTABLES} [-c {COMMANDS}] [-f {FORMAT}] [-v]",
                         "       parallel -e Crm:Contests -c install:start -f .\\Services\\{0}.exe")]

[assembly: ComVisible(false)]
[assembly: Guid("fd36d286-d224-4873-b4a5-ad1b59bea82a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.*")]
