﻿using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;

namespace Parallel
{
    internal class Options
    {
        [OptionList('e', "executables", Required = true, HelpText = "Specify the executables paths (or names, see 'format')")]
        public IList<string> Executables { get; set; } = new List<string>();

        [OptionList('c', "commands", HelpText = "Specify commands to send to the executables. If ommited, the executables are simply launched.")]
        public IList<string> Commands { get; set; } = new List<string>();

        [Option('f', "format", HelpText = "If provided, each executable gets applied to this string")]
        public string Format { get; set; }

        [Option('v', "verbose", HelpText = "Print more details during execution")]
        public bool Verbose { get; set; }

        [HelpOption]
        public string GetUsage() => HelpText.AutoBuild(this, c => HelpText.DefaultParsingErrorsHandler(this, c));
    }
}
