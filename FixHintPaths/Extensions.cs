﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Core.Model;
using log4net;

namespace FixHintPaths
{
    public static class Extensions
    {
        private static string GetFixedPrefix(this Project project, FileSystemInfo solutionDirectory)
        {
            var n = project.Path.Replace(solutionDirectory.FullName, "").TrimStart('\\').Count(c => c == '\\');
            return $"{string.Concat(Enumerable.Repeat(@"..\", n))}packages";
        }

        public static void FixHintPathsPrefixes(this Project project, DirectoryInfo solutionDirectory, ILog log)
        {
            log.Info($"[{project.Name}]");

            var fixedPrefix = project.GetFixedPrefix(solutionDirectory);

            log.Info($"  Correct hint path prefix: {fixedPrefix}");

            var content = File.ReadAllText(project.Path);
            var amountFixed = 0;

            Func<string, MatchEvaluator> evaluator = correct => match =>
            {
                if (match.Value != correct)
                {
                    amountFixed++;
                    log.Info($"    Found incorrect prefix: {match.Value}");
                }

                return correct;
            };

            var nugetEvaluator = evaluator($"<HintPath>{fixedPrefix}");
            var projectEvaluator = evaluator($"Project=\"{fixedPrefix}");
            var projectConditionEvaluator = evaluator($"Condition=\"Exists('{fixedPrefix}");


            content = Regex.Replace(content, @"<HintPath>(\d|\w|\s|\.|\\)*packages", nugetEvaluator);
            content = Regex.Replace(content, @"<HintPath>\$\(SolutionDir\)packages", nugetEvaluator);

            content = Regex.Replace(content, $"Project=\"{@"(\d|\w|\s|\.|\\)"}*packages", projectEvaluator);
            content = Regex.Replace(content, $"Condition=\"Exists\\('{@"(\d|\w|\s|\.|\\)"}*packages", projectConditionEvaluator);

            File.WriteAllText(project.Path, content);

            log.Info($"  Fixed {amountFixed} hint paths");
        }
    }
}
