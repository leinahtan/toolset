﻿using System;
using System.Diagnostics;

namespace Core
{
    public class Batch
    {
        public const string DefaultShell = "cmd.exe";

        public string Shell { get; }
        public Action<string> OnOutput { get; set; } = s => {};
        public Action<string> OnError { get; set; } = s => {};

        public Batch(string shell)
        {
            Shell = shell;
        }

        public Batch() : this(DefaultShell) {}

        public int Evaluate(string command) => Evaluate(command, OnOutput, OnError, Shell);
        public int Evaluate(string command, Action<string> onOutput) => Evaluate(command, onOutput, OnError, Shell);

        public int Evaluate(string command, Action<string> onOutput, Action<string> onError)
            => Evaluate(command, onOutput, onError, Shell);

        public static int Evaluate(string command, Action<string> onOutput, Action<string> onError, string shell = DefaultShell)
        {
            using (var process = Process.Start(CreateProcess(command, shell)))
            {
                if (process == null)
                {
                    throw new Exception("Could not start the process");
                }

                process.OutputDataReceived += (sender, e) => { if (e.Data != null) onOutput(e.Data); };
                process.ErrorDataReceived += (sender, e) => { if (e.Data != null) onError(e.Data); };

                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();

                return process.ExitCode;
            }
        }

        private static ProcessStartInfo CreateProcess(string command, string shell)
            => new ProcessStartInfo(shell, $"/c {command}")
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };
    }
}
