﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Model;
using Microsoft.Build.Construction;

namespace Core
{
    public static class ProjectsLoader
    {
        public static List<Project> FromSolution(string solution)
            => SolutionFile.Parse(solution)
                           .ProjectsInOrder
                           .Where(p => p.ProjectType == SolutionProjectType.KnownToBeMSBuildFormat)
                           .OrderBy(p => p.ProjectName)
                           .Select(ProjectBuilder.From)
                           .ToList();

        public static List<Project> FromSolution(FileInfo solution) => FromSolution(solution.FullName);

        public static List<Project> FromDirectory(string path)
            => new DirectoryInfo(path).GetAllFiles("*.csproj", "*.vbproj").Select(ProjectBuilder.From).ToList();
    }

    public static class ProjectBuilder
    {
        public static Project From(ProjectInSolution p) => new Project(p.ProjectName, p.AbsolutePath);

        public static Project From(FileInfo f) => new Project(f.Name.StripEnd("."), f.FullName);
    }
}
