﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class Argument
    {
        public string Name { get; }
        public bool ExpectInput { get; }

        public object Value { get; private set; }
        public bool HaveValue { get; private set; }
        public bool Processed { get; private set; }

        private readonly Action<string> _process;

        public Argument(string name, bool expectInput)
        {
            Name = name;
            ExpectInput = expectInput;
        }

        public Argument(string name, Func<string, object> manager) : this(name, true)
        {
            _process = input =>
            {
                Value = manager(input);
                Processed = HaveValue = true;
            };
        }

        public Argument(string name, Action<string> manager) : this(name, true)
        {
            _process = input =>
            {
                manager(input);
                Processed = true;
            };
        }

        public Argument(string name, Action manager) : this(name, false)
        {
            _process = input =>
            {
                manager();
                Processed = true;
            };
        }

        public void Process(string value) => _process(value);
        public void Process() => _process(null);
    }

    public class ArgumentsManager
    {
        public IDictionary<string, Argument> Args { get; } = new Dictionary<string, Argument>();

        public void Add(string name, Func<string, object> manage) => Args[name] = new Argument(name, manage);
        public void Add(string name, Action<string> manage) => Args[name] = new Argument(name, manage);
        public void Add(string name, Action manage) => Args[name] = new Argument(name, manage);
        public void Add(string name) => Add(name, value => value);

        public List<string> Manage(IEnumerable<string> args)
        {
            var nonManaged = new List<string>();

            foreach (var tokens in args.Select(arg => arg.Split(new[] { '=' }, 2)))
            {
                var name = tokens[0];

                if (!Args.ContainsKey(name))
                {
                    nonManaged.Add(string.Join("=", tokens));
                    continue;
                }

                var arg = Args[name];

                if (arg.Processed)
                {
                    throw new Exception($"Argument {arg.Name} was found more than once");
                }

                arg.Process(tokens.Length > 1 ? tokens[1] : null);
            }

            return nonManaged;
        }
    }
}
