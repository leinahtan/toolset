﻿using System.IO;

namespace Core.Model
{
    public class Project
    {
        public string Name { get; }
        public string Path { get; }

        public Project(string name, string path)
        {
            Name = name;
            Path = path;
        }

        public DirectoryInfo Directory => new FileInfo(Path).Directory;
    }
}
