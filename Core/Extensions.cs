﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Core
{
    public static class Extensions
    {
        public static DirectoryInfo CrawlUntilFileMatch(this DirectoryInfo directory, string pattern)
        {
            while (directory != null && directory.GetFiles(pattern).Length == 0)
            {
                directory = directory.Parent;
            }

            if (directory == null)
            {
                throw new DirectoryNotFoundException($"Cannot find directory containing file matching '{pattern}'");
            }

            return directory;
        }

        public static IEnumerable<FileInfo> GetAllFiles(this DirectoryInfo directory, params string[] patterns) =>
            patterns.Select(p => directory.GetFiles(p, SearchOption.AllDirectories))
                    .SelectMany(x => x)
                    .Distinct()
                    .ToList();

        public static string StripEnd(this string str, string after, bool keep = false)
        {
            var lastAfter = str.LastIndexOf(after, StringComparison.Ordinal);
            return str.Substring(0, str.Length - (str.Length - lastAfter) + (keep ? 1 : 0));
        }

        public static DirectoryInfo GetDirectory(this DirectoryInfo directory, string path)
            => new DirectoryInfo(Path.Combine(directory.FullName, path));

        public static FileInfo GetFile(this DirectoryInfo directory, string path)
            => new FileInfo(Path.Combine(directory.FullName, path));

        public static bool TryDelete(this DirectoryInfo directory) 
        {
            try
            {
                directory.Delete(true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsNullOrEmpty(this string s) => string.IsNullOrEmpty(s);

        public static bool IsNullOrWhiteSpace(this string s) => string.IsNullOrWhiteSpace(s);

        public static bool HasValue(this string s) => !string.IsNullOrEmpty(s);
    }
}
