﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core;
using log4net;
using log4net.Config;

namespace Test
{
    internal class Program
    {
        private static ILog Log { get; } = LogManager.GetLogger(typeof(Program));
        private static Batch Batch => _batch.Value;

        private static readonly Lazy<Batch> _batch = new Lazy<Batch>(() =>
        {
            var batch = new Batch();

            //if (!Options.Verbose) return batch;

            batch.OnOutput = x => Log.Info($" | {x}");
            batch.OnError = x => Log.Error($" | {x}");

            return batch;
        });

        static Program()
        {
            XmlConfigurator.Configure();
        }

        private static void Main(string[] args)
        {
            var sw = Stopwatch.StartNew();

            const string format = @"C:\Dev\buzzmath\__PUBLISHED_Debug\Services\{0}.WindowsService\bin\{0}.WindowsService.exe";
            
            var services = new[]
            {
                "AccessAndRights",
                "Accounts",
                "Assignments",
                "Contests",
                "Crm",
                "Customers",
                "DataWarehouse",
                "Messaging",
                "Organizations",
                "Publisher",
                "SearchProxy",
                "Workbook"
            }.Select(name => new Service(name, string.Format(format, name))).ToList();

            var tasks = services.Select(s => SendCommands(s, "stop", "uninstall"));

            Task.WaitAll(tasks.ToArray());

            sw.Stop();

            Console.WriteLine($"Time Elapsed: {sw.ElapsedMilliseconds}ms");

            Console.WriteLine("end");
        }

        private static Task<int> SendCommand(Service service, string command)
        {
            return Task.Run(() => Batch.Evaluate($"{service.Binary.FullName} {command}"/*, s => Console.WriteLine($" | [{service.Name}]: {s}")*/));
        }

        private static async Task SendCommands(Service service, params string[] commands)
        {
            foreach (var command in commands)
            {
                Log.Info($"[{service.Name}]: '{command}' - Sending...");

                var exit = await SendCommand(service, command);

                if (exit != 0) throw new Exception($"[{service.Name}] Non zero exit ({exit})");

                Log.Info($"[{service.Name}]: '{command}' - Success");
            }
        }
    }

    internal class Service
    {
        public string Name { get; }
        public FileInfo Binary { get; }

        public Service(string name, string path)
        {
            Name = name;
            Binary = new FileInfo(path);
        }
    }
}
